#pragma once

#include <vector>
#include <iostream>
#include <algorithm>
#include <iterator>

using namespace std;

pair<vector<int>::const_iterator, vector<int>::const_iterator> rasporediBrojeve(vector<int> &brojevi)
{
    const auto krajPrveParticije = stable_partition(begin(brojevi), end(brojevi), [](const auto &broj)
                                                    { return broj % 3 == 0; });
    const auto krajDrugeParticije = stable_partition(krajPrveParticije, end(brojevi), [](const auto &broj)
                                                     { return broj % 3 == 1; });
    return {krajPrveParticije, krajDrugeParticije};
}
