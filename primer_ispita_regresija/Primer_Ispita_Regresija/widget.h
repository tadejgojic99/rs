#ifndef WIDGET_H
#define WIDGET_H

#include "solver.h"
#include <QWidget>

QT_BEGIN_NAMESPACE
namespace Ui { class Widget; }
QT_END_NAMESPACE

class Widget : public QWidget
{
    Q_OBJECT

public:
    Widget(QWidget *parent = nullptr);
    ~Widget();

public slots:
    void on_optimizuj_pushButton_clicked();
    void onUpdate(double greska, QString string);
    void onFinish();

private:
    Ui::Widget *ui;
    std::vector<double> m_xs;
    std::vector<double> m_ys;
};
#endif // WIDGET_H
