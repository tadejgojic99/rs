#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>

QT_BEGIN_NAMESPACE
namespace Ui { class Widget; }
QT_END_NAMESPACE

class Widget : public QWidget
{
    Q_OBJECT

public:
    Widget(QWidget *parent = nullptr);
    ~Widget();

public slots:
    void on_generisi_pushButton_clicked();
    void on_zapocni_pushButton_clicked();

private:
    Ui::Widget *ui;
    std::vector<unsigned int> v;
};
#endif // WIDGET_H
