#include "town.h"
#include <QRandomGenerator>

Town::Town(int n, QObject *parent)
    : QObject(parent)
{
    for (auto i = 0; i < n; ++i)
    {
        QVector<int> row;
        for (auto j = 0; j < n; ++j)
        {
            const auto randomValue = QRandomGenerator::global()->bounded(-50, 50);
            row.push_back(randomValue);
        }
        _matrix.push_back(row);
    }
}

int Town::numberOfRowsAndColumns() const
{
    return _matrix.size();
}

double Town::cellValue(int i, int j) const
{
    return _matrix[i][j];
}

void Town::updateNewValueForCell(int i, int j, int value)
{
    _matrix[i][j] += value;
}

bool Town::hasNegative(int index) const
{
    for(const auto &x : _matrix.at(index))
    {
        if(x < 0)
            return true;
    }

    return false;
}
