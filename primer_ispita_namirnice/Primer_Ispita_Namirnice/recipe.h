#ifndef RECIPE_H
#define RECIPE_H
\
#include <QString>
#include <QVector>
#include <QVariant>

class Recipe
{
public:
    Recipe(const QString &name = nullptr);
    void fromVariant(const QVariant &variant);
    QString toQString();

    QVector<QString> list();
    QString current();
    void setCurrent(const QString &s);
    void setCurrent(const unsigned int &index, const QChar &value);
    QVector<bool> bought();
    void setBought(const QVector<bool> &v);
    void setBought(const unsigned int &index, const bool &value);

private:
    QString m_name;
    QVector<QString> m_list;
    QString m_current;
    QVector<bool> m_bought;
};

#endif // RECIPE_H
