#include <string>
#include "palindrom.hpp"

using namespace std;

int main()
{
    string test = "anavolimilovana";
    cout << test << (palindrom(test) ? " jeste " : " nije ") << "palindrom" << endl;

    test = "test";
    cout << test << (palindrom(test) ? " jeste " : " nije ") << "palindrom" << endl;

    return 0;
}
