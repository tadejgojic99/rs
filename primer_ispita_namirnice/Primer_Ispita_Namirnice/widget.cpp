#include "widget.h"
#include "ui_widget.h"
#include "recipethread.h"

Widget::Widget(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::Widget)
{
    ui->setupUi(this);
}

Widget::~Widget()
{
    delete ui;
//    qDeleteAll(m_list);
    qDeleteAll(m_recipes);
}

void Widget::on_ucitaj_pushButton_clicked()
{
    const QString path = QFileDialog::getOpenFileName(this, "Otvori JSON", "../", "JSON files(*.json)");
    QFile file(path);
    file.open(QFile::ReadOnly);
    const auto document = QJsonDocument::fromJson(file.readAll());
    const auto variantData = document.toVariant();

    QVariantList list = variantData.toList();
    for(const auto &e : list)
    {
        Recipe *recipe = new Recipe();
        recipe->fromVariant(e);
        m_recipes.append(recipe);
        QString tmpString = "";
        for(int i = 0; i < recipe->list().size(); ++i)
        {
            tmpString.append('X');
        }
        recipe->setCurrent(tmpString);
        ui->teSpisak->append(recipe->toQString());
    }

    file.close();
}

void Widget::on_zapocni_pushButton_clicked()
{
    const unsigned int brojKupaca = ui->lineEdit->text().toUInt();

    for(unsigned index=0; index < brojKupaca; ++index)
    {
        auto thread = new RecipeThread(m_recipes, index, &m_mutex);

        QObject::connect(thread, &RecipeThread::buy,
                         this, &Widget::onBuy);
        QObject::connect(thread, &RecipeThread::finished,
                         thread, &RecipeThread::deleteLater);

        thread->start();
    }
}

void Widget::onBuy(const unsigned int &i, const unsigned int &j)
{
        m_recipes[i]->setCurrent(j, 'O');
        ui->teSpisak->setText("");
        for(const auto &r : m_recipes)
        {
            ui->teSpisak->append(r->toQString());
        }
}
