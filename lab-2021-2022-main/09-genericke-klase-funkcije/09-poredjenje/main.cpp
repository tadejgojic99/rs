#include <iostream>
#include <string>
#include <iomanip>

#include "Par.hpp"

int main()
{
    std::cout << std::boolalpha;

    const auto p1 = matf::napravi_par(std::string{"Tekst"}, 3);
    const auto p2 = matf::napravi_par(std::string{"Tekst"}, -3);

    // Sada sve radi kako bi trebalo
    std::cout << (p1 == p1) << std::endl;
    std::cout << (p1 != p1) << std::endl;
    std::cout << (p1 == p2) << std::endl;
    std::cout << (p1 != p2) << std::endl;

    return 0;
}
