#pragma once

#include <iostream>
#include <string>
#include <numeric>
#include <vector>
#include <iterator>

using namespace std;

bool proveriUredjenost(const string &tekst)
{
    return inner_product(
        cbegin(tekst),
        cend(tekst) - 1,
        cbegin(tekst) + 1,
        true,
        [](const auto &akumulator, const auto &jednaki)
        { return akumulator && jednaki; },
        [](const auto &leviKarakter, const auto &desniKarakter)
        { return leviKarakter <= desniKarakter; });
}