#ifndef LINEARREGRESSION_H
#define LINEARREGRESSION_H

#include <QString>
#include <vector>
#include <math.h>

class LinearRegression
{
public:
    LinearRegression(double a, double b);
    double a();
    double b();
    QString toQString();
    double error(std::vector<double> xs, std::vector<double> ys);
    double predict(double x);

private:
    double m_a;
    double m_b;
};

#endif // LINEARREGRESSION_H
