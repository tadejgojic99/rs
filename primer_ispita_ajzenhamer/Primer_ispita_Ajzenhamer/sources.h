#ifndef SOURCES_H
#define SOURCES_H

#include <QString>
#include <QVariant>

class Sources
{
public:
    Sources();
    Sources(const QVariant&);
    unsigned int getVolume() const;

    void fromQVariant(const QVariant&);
    QString toQString() const;
    bool isEmpty() const;
    void releaseFluid(unsigned int);

private:
    QString m_name;
    unsigned int m_volume;
};

#endif // SOURCES_H
