#pragma once

#include "Poredjenje.hpp"

namespace matf
{
    // Pravimo klasu koja ce testirati implementirane operatore.
    // Primetimo da klasa `Par` prosledjuje sebe prilikom nasledjivanja sablona `Poredjenje`.
    // Deluje da nema smisla zasto bismo ovo radili,
    // ali je neophodno da bi se prilikom poziva nekog od operatora iz sablona `Poredjenje`
    // mogla izvrsiti staticka konverzija u objekat odgovarajuce klase.
    template <typename T1, typename T2>
    class Par : public Poredjenje<matf::Par<T1, T2>>
    {
    public:
        Par(T1 t1, T2 t2)
            : prvaVrednost(t1), drugaVrednost(t2)
        {
        }

        Par() = default;

        void zameni(Par<T1, T2> &drugi)
        {
            std::swap(prvaVrednost, drugi.prvaVrednost);
            std::swap(drugaVrednost, drugi.drugaVrednost);
        }

        // Ovde definisemo samo operator==,
        // a operator!= ce biti koriscen iz bazne klase.
        bool operator==(const Par &drugi) const
        {
            return prvaVrednost == drugi.prvaVrednost && drugaVrednost == drugi.drugaVrednost;
        }

        T1 prvaVrednost;
        T2 drugaVrednost;
    };

    template <typename T1, typename T2>
    Par<T1, T2> napravi_par(T1 t1, T2 t2)
    {
        return Par<T1, T2>(t1, t2);
    }

} // namespace matf
