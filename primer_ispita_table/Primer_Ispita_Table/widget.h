#ifndef WIDGET_H
#define WIDGET_H

#include "town.h"
#include <QWidget>
#include <QMutexLocker>

QT_BEGIN_NAMESPACE
namespace Ui { class Widget; }
QT_END_NAMESPACE

class Widget : public QWidget
{
    Q_OBJECT

public:
    Widget(QWidget *parent = nullptr);
    ~Widget();
    void populateTable();
    Town &getTown();
    QMutex &getMutexForTown();

public slots:
    void on_zapocniDonacije_pushButton_clicked();
    void onThreadFinished(int i, int val);

private slots:
    void startThreads();

private:
    Ui::Widget *ui;
    Town *_town;
    QMutex _mutexForTown;
};
#endif // WIDGET_H
