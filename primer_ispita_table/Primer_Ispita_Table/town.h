#ifndef TOWN_H
#define TOWN_H

#include <QObject>

class Town : public QObject
{
    Q_OBJECT
public:
    explicit Town(int n, QObject *parent = nullptr);
    Town(const Town &) = delete;
    Town& operator=(const Town &) = delete;
    Town(Town &&) = delete;
    Town& operator=(Town &&) = delete;

    int numberOfRowsAndColumns() const;
    double cellValue(int i, int j) const;
    void updateNewValueForCell(int i, int j, int value);
    bool hasNegative(int index) const;

private:
    QVector<QVector<int>> _matrix;
};

#endif // TOWN_H
