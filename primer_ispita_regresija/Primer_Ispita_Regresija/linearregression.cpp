#include "linearregression.h"

LinearRegression::LinearRegression(double a, double b) : m_a(a), m_b(b)
{

}

double LinearRegression::predict(double x)
{
    double tmp = m_a + x*m_b;
    return tmp;
}

double LinearRegression::a()
{
    return m_a;
}

double LinearRegression::b()
{
    return m_b;
}

QString LinearRegression::toQString()
{
    return "f(x) = " + QString::number(m_a) + " + " + QString::number(m_b) +  "x";
}

double LinearRegression::error(std::vector<double> xs, std::vector<double> ys)
{
    int n = static_cast<int>(xs.size());
    double sigma = 0.0;
    for(int i = 0; i < n; ++i)
    {
        double tmp1 = ys.at(i) - predict(xs.at(i));
        sigma += tmp1 * tmp1;
    }

    return sigma;
}
