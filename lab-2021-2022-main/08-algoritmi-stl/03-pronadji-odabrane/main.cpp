#include <vector>
#include <iostream>
#include <iterator>
#include <string>
#include "pronadjiOdabrane.hpp"

using namespace std;

int main()
{
    const vector<string> stavke{
        "aardvark",
        "compunctious",
        "**congratulations**",
        "contrafribblarity",
        "contrary",
        "dictionary",
        "**interphrastical**",
        "**patronise**",
        "**pericombobulation**",
        "phrasmotic",
        "**syllables**"};

    const auto indikatori = pronadjiOdabrane(stavke);
    copy(cbegin(indikatori), cend(indikatori), ostream_iterator<bool>(cout, "\n"));

    return 0;
}
