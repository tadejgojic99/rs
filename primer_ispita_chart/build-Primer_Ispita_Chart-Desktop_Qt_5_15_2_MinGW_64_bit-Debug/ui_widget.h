/********************************************************************************
** Form generated from reading UI file 'widget.ui'
**
** Created by: Qt User Interface Compiler version 5.15.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_WIDGET_H
#define UI_WIDGET_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QGraphicsView>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Widget
{
public:
    QVBoxLayout *verticalLayout_4;
    QGroupBox *groupBox;
    QVBoxLayout *verticalLayout_3;
    QLabel *label_2;
    QHBoxLayout *horizontalLayout;
    QLabel *ukupno_label;
    QLineEdit *ukupno_lineEdit;
    QPushButton *generisi_pushButton;
    QPushButton *zapocni_pushButton;
    QGraphicsView *graphicsView;

    void setupUi(QWidget *Widget)
    {
        if (Widget->objectName().isEmpty())
            Widget->setObjectName(QString::fromUtf8("Widget"));
        Widget->resize(981, 718);
        verticalLayout_4 = new QVBoxLayout(Widget);
        verticalLayout_4->setObjectName(QString::fromUtf8("verticalLayout_4"));
        groupBox = new QGroupBox(Widget);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        QFont font;
        font.setPointSize(14);
        font.setStrikeOut(false);
        font.setKerning(true);
        groupBox->setFont(font);
        groupBox->setAlignment(Qt::AlignBottom|Qt::AlignHCenter);
        verticalLayout_3 = new QVBoxLayout(groupBox);
        verticalLayout_3->setObjectName(QString::fromUtf8("verticalLayout_3"));
        label_2 = new QLabel(groupBox);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        QFont font1;
        font1.setPointSize(14);
        label_2->setFont(font1);
        label_2->setAlignment(Qt::AlignCenter);

        verticalLayout_3->addWidget(label_2);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        ukupno_label = new QLabel(groupBox);
        ukupno_label->setObjectName(QString::fromUtf8("ukupno_label"));
        QFont font2;
        font2.setPointSize(12);
        font2.setStrikeOut(false);
        font2.setKerning(true);
        ukupno_label->setFont(font2);

        horizontalLayout->addWidget(ukupno_label);

        ukupno_lineEdit = new QLineEdit(groupBox);
        ukupno_lineEdit->setObjectName(QString::fromUtf8("ukupno_lineEdit"));

        horizontalLayout->addWidget(ukupno_lineEdit);

        generisi_pushButton = new QPushButton(groupBox);
        generisi_pushButton->setObjectName(QString::fromUtf8("generisi_pushButton"));

        horizontalLayout->addWidget(generisi_pushButton);

        zapocni_pushButton = new QPushButton(groupBox);
        zapocni_pushButton->setObjectName(QString::fromUtf8("zapocni_pushButton"));

        horizontalLayout->addWidget(zapocni_pushButton);


        verticalLayout_3->addLayout(horizontalLayout);


        verticalLayout_4->addWidget(groupBox);

        graphicsView = new QGraphicsView(Widget);
        graphicsView->setObjectName(QString::fromUtf8("graphicsView"));

        verticalLayout_4->addWidget(graphicsView);


        retranslateUi(Widget);

        QMetaObject::connectSlotsByName(Widget);
    } // setupUi

    void retranslateUi(QWidget *Widget)
    {
        Widget->setWindowTitle(QCoreApplication::translate("Widget", "Widget", nullptr));
        groupBox->setTitle(QString());
        label_2->setText(QCoreApplication::translate("Widget", "Kontrole", nullptr));
        ukupno_label->setText(QCoreApplication::translate("Widget", "Ukupno:", nullptr));
        generisi_pushButton->setText(QCoreApplication::translate("Widget", "Generisi resurse", nullptr));
        zapocni_pushButton->setText(QCoreApplication::translate("Widget", "Zapocni razmene", nullptr));
    } // retranslateUi

};

namespace Ui {
    class Widget: public Ui_Widget {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_WIDGET_H
