#pragma once

#include <string>
#include "HttpZahtev.hpp"

class FunkcijaSrednjegSloja
{
public:
    FunkcijaSrednjegSloja(std::string deoZahteva, std::string sadrzaj)
        : _deoZahteva(deoZahteva), _sadrzaj(sadrzaj)
    {
    }

private:
    friend HttpZahtev &operator+(HttpZahtev &&zahtev, const FunkcijaSrednjegSloja &f)
    {
        zahtev.dodajSadrzaj(f._deoZahteva, f._sadrzaj);
        return zahtev;
    }

    std::string _deoZahteva;
    std::string _sadrzaj;
};
