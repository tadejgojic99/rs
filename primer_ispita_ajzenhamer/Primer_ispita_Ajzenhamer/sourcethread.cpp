#include "sourcethread.h"

SourceThread::SourceThread(const QVector<Sources *> sources, QMutex *mutex, QObject *parent) : QThread(parent),
    m_sources(sources), m_mutex(mutex)
{}

void SourceThread::run()
{
    for(;;)
    {
        const auto ms = (QRandomGenerator::global()->generate() % 6) * 100u + 500u;
        msleep(ms);

        QMutexLocker lock(m_mutex);
        const auto i = QRandomGenerator::global()->generate() % m_sources.size();

        const auto fluidLeft = m_sources[i]->getVolume();
        if(fluidLeft == 0u)
        {
            continue;
        }

        const auto fluidLost = qMin((QRandomGenerator::global()->generate() % 101) + 100, fluidLeft);
        m_sources[i]->releaseFluid(fluidLost);

        emit sourceLostFluid(i, fluidLost);

        if(m_sources[i]->getVolume() == 0)
        {
            break;
        }
    }
}

