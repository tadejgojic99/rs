#include "city.h"

City::City(const QString &name, const double &x, const double &y) :
    m_name(name), m_x(x), m_y(y), m_visited(false)
{

}

City::City(const QVariant &variant)
{
    fromQVariant(variant);
}

void City::fromQVariant(const QVariant &variant)
{
    const QVariantMap map = variant.toMap();

    m_name = map.value("name").toString();
    m_x = map.value("x").toDouble();
    m_y = map.value("y").toDouble();
}

QString City::toQString()
{
    return m_name + " ---> " + "(" + QString::number(m_x) + ", " + QString::number(m_y) + ")";
}

bool City::visited()
{
    return m_visited;
}

void City::visit()
{
    m_visited = true;
}

double City::distance(const City &other) const
{
    return std::sqrt(pow((m_x - other.m_x), 2) + pow((m_y - other.m_y), 2));
}

QString City::name() const
{
    return m_name;
}
