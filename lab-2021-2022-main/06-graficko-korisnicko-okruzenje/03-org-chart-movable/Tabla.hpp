#ifndef TABLA_HPP
#define TABLA_HPP

#include <QGraphicsScene>

class Zaposleni;
class CvorZaposleni;

class Tabla : public QGraphicsScene
{
public:
    explicit Tabla(QObject *parent = nullptr);

public slots:
    void DodatZaposleniNaTablu(CvorZaposleni *cvor);

private:
    void PozicionirajNoviCvor(CvorZaposleni *cvor);
    void NacrtajNoveVeze();

    // Zarad ubrzanja u implementaciji
    QVector<CvorZaposleni *> _sefovi;
    QVector<CvorZaposleni *> _radnici;
    QVector<QGraphicsLineItem *> _veze;
};

#endif // TABLA_HPP
