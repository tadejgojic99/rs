#pragma once

#include <set>
#include <stdexcept>
#include <optional>
#include "Porodica.hpp"

using namespace std;

class LokatorSupermarketa
{
public:
    LokatorSupermarketa(double x, double y)
        : _x(x), _y(y)
    {
    }

    size_t dodajPorodicu(const Porodica &p)
    {
        _porodice.insert(p);
        return _porodice.size();
    }

    optional<double> operator()(unsigned kvadrant) const
    {
        proveriKvadrant(kvadrant);

        const auto porodiceIzKvadranta = izdvojPorodiceIzKvadranta(kvadrant);
        if (porodiceIzKvadranta.empty())
        {
            return {};
        }

        return sumirajRastojanjaDoPorodica(porodiceIzKvadranta);
    } 

private:
    set<Porodica> izdvojPorodiceIzKvadranta(unsigned kvadrant) const
    {
        set<Porodica> porodiceIzKvadranta;
        for (auto iter = _porodice.cbegin(); iter != _porodice.cend(); ++iter)
        {
            if (iter->ziviUKvadrantu(kvadrant)) 
            {
                porodiceIzKvadranta.insert(*iter);
            }
        }
        return porodiceIzKvadranta;
    }

    double sumirajRastojanjaDoPorodica(const set<Porodica> &porodice) const
    {
        auto sumaRastojanja = 0.0;
        for (auto iter = porodice.cbegin(); iter != porodice.cend(); ++iter)
        {
            azurirajSumuRastojanjaZaPorodicu(*iter, sumaRastojanja);
        }

        return sumaRastojanja;
    }

    void azurirajSumuRastojanjaZaPorodicu(const Porodica &porodica, double &sumaRastojanja) const
    {
        const auto rastojanjeDoPorodice = porodica.rastojanjeDo(_x, _y);
        sumaRastojanja += rastojanjeDoPorodice;
    }

    void proveriKvadrant(unsigned kvadrant) const
    {
        if (kvadrant != 1u && kvadrant != 2u && kvadrant != 3u && kvadrant != 4u)
        {
            throw invalid_argument("kvadrant");
        }
    }

    double _x;
    double _y;
    set<Porodica> _porodice;
};
