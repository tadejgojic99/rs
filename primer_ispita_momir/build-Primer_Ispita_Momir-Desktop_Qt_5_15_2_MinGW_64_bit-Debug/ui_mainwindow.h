/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.15.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QTextEdit>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralwidget;
    QGridLayout *gridLayout;
    QVBoxLayout *verticalLayout;
    QPushButton *ucitajGradove_pushButton;
    QLabel *gradoviLabel;
    QTextEdit *gradovi_textEdit;
    QVBoxLayout *verticalLayout_2;
    QPushButton *putevi_pushButton;
    QLabel *puteviLabel;
    QTextEdit *putevi_textEdit;
    QMenuBar *menubar;
    QStatusBar *statusbar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
        MainWindow->resize(974, 669);
        centralwidget = new QWidget(MainWindow);
        centralwidget->setObjectName(QString::fromUtf8("centralwidget"));
        gridLayout = new QGridLayout(centralwidget);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        verticalLayout = new QVBoxLayout();
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        verticalLayout->setContentsMargins(-1, -1, -1, 6);
        ucitajGradove_pushButton = new QPushButton(centralwidget);
        ucitajGradove_pushButton->setObjectName(QString::fromUtf8("ucitajGradove_pushButton"));

        verticalLayout->addWidget(ucitajGradove_pushButton);

        gradoviLabel = new QLabel(centralwidget);
        gradoviLabel->setObjectName(QString::fromUtf8("gradoviLabel"));
        gradoviLabel->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        verticalLayout->addWidget(gradoviLabel);

        gradovi_textEdit = new QTextEdit(centralwidget);
        gradovi_textEdit->setObjectName(QString::fromUtf8("gradovi_textEdit"));

        verticalLayout->addWidget(gradovi_textEdit);


        gridLayout->addLayout(verticalLayout, 0, 0, 1, 1);

        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        putevi_pushButton = new QPushButton(centralwidget);
        putevi_pushButton->setObjectName(QString::fromUtf8("putevi_pushButton"));

        verticalLayout_2->addWidget(putevi_pushButton);

        puteviLabel = new QLabel(centralwidget);
        puteviLabel->setObjectName(QString::fromUtf8("puteviLabel"));

        verticalLayout_2->addWidget(puteviLabel);

        putevi_textEdit = new QTextEdit(centralwidget);
        putevi_textEdit->setObjectName(QString::fromUtf8("putevi_textEdit"));

        verticalLayout_2->addWidget(putevi_textEdit);


        gridLayout->addLayout(verticalLayout_2, 1, 0, 1, 1);

        MainWindow->setCentralWidget(centralwidget);
        menubar = new QMenuBar(MainWindow);
        menubar->setObjectName(QString::fromUtf8("menubar"));
        menubar->setGeometry(QRect(0, 0, 974, 22));
        MainWindow->setMenuBar(menubar);
        statusbar = new QStatusBar(MainWindow);
        statusbar->setObjectName(QString::fromUtf8("statusbar"));
        MainWindow->setStatusBar(statusbar);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QCoreApplication::translate("MainWindow", "MainWindow", nullptr));
        ucitajGradove_pushButton->setText(QCoreApplication::translate("MainWindow", "Ucitaj gradove", nullptr));
        gradoviLabel->setText(QCoreApplication::translate("MainWindow", "Gradovi", nullptr));
        putevi_pushButton->setText(QCoreApplication::translate("MainWindow", "Izgradi puteve", nullptr));
        puteviLabel->setText(QCoreApplication::translate("MainWindow", "Putevi", nullptr));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
