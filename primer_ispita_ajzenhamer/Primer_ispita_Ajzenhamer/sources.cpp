#include "sources.h"

Sources::Sources()
{

}

Sources::Sources(const QVariant &variant)
{
    fromQVariant(variant);
}

unsigned int Sources::getVolume() const
{
    return m_volume;
}

void Sources::releaseFluid(unsigned int newVolume)
{
    m_volume -= newVolume;
}

void Sources::fromQVariant(const QVariant &variant)
{
    const QVariantMap map = variant.toMap();
    m_name = map.value("name").toString();
    m_volume = map.value("volume").toUInt();
}

QString Sources::toQString() const
{
    QString volume = isEmpty()
                   ? "izvor je iscrpljen"
                   : QString::number(m_volume);
    return m_name + " - " + volume;
}

bool Sources::isEmpty() const
{
    return m_volume == 0u;
}

