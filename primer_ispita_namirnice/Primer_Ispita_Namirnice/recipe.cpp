#include "recipe.h"

Recipe::Recipe(const QString &name) : m_name(name), m_list({}), m_current("")
{

}

QVector<QString> Recipe::list()
{
    return m_list;
}

void Recipe::fromVariant(const QVariant &variant)
{
    QVariantMap map = variant.toMap();

    m_name = map.value("title").toString();
    const auto tmp = map.value(" ingredients ").toList();
    for(const auto &e : tmp)
    {
        m_list.append(e.toString());
    }

    m_bought.fill(false, m_list.size());
}

QString Recipe::toQString()
{
    const QString tmp1 = m_name.trimmed();
    return tmp1 + "\n" + m_current + "\n";
}

QVector<bool> Recipe::bought() {
    return m_bought;
}

void Recipe::setBought(const QVector<bool> &v)
{
    m_bought = v;
}

void Recipe::setBought(const unsigned int &index, const bool &value)
{
    m_bought[index] = value;
}

void Recipe::setCurrent(const QString &s)
{
    m_current = s;
}
void Recipe::setCurrent(const unsigned int &index, const QChar &value)
{
    m_current[index] = value;
}
