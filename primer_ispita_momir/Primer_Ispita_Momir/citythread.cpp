#include "citythread.h"
#include <QDebug>

CityThread::CityThread(QVector<City *> cities, unsigned int id, QObject *parent) : QThread(parent), m_cities(cities), m_id(id)
{

}

void CityThread::run()
{
    trace("Started...");

    const auto myCity = m_cities.at(m_id);
    myCity->visit();

    msleep(500);

    double shortestDistance = static_cast<double>(INFINITY);
    QString shortestTown = "";
    int nextId = m_id;
    for(int i = 0; i < m_cities.size(); ++i)
    {
        if(static_cast<unsigned int>(i) != m_id && !m_cities.at(i)->visited())
        {
            double tmp = myCity->distance(*m_cities.at(i));
            if(tmp < shortestDistance)
            {
                shortestDistance = tmp;
                shortestTown = m_cities.at(i)->name();
                nextId = i;
            }
        }
    }

    emit buildRoad(m_id, nextId);
}

void CityThread::trace(const char *msg) const
{
    qDebug() << "Thread " + QString::number(m_id) + ": " + msg;
}
