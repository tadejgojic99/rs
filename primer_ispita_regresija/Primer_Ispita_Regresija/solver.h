#ifndef SOLVER_H
#define SOLVER_H

#include "linearregression.h"
#include <QThread>

class Solver : public QThread
{
    Q_OBJECT
public:
    Solver(double min, double max, double increment, std::vector<double> xs, std::vector<double> ys);

protected:
    void run() override;

signals:
    void update(double error, QString string);

private:
    double m_min;
    double m_max;
    double m_increment;
    std::vector<double> m_xs;
    std::vector<double> m_ys;
};

#endif // SOLVER_H
