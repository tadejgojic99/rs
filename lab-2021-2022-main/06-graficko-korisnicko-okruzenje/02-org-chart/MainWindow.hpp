#ifndef MAINWINDOW_HPP
#define MAINWINDOW_HPP

#include <QWidget>
class QGraphicsScene;
class Zaposleni;
class CvorZaposleni;

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QWidget
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

signals:
    void DodatNoviRadnik(CvorZaposleni *);

private slots:
    void DodajNovogZaposlenog();

private:
    Ui::MainWindow *ui;
    QGraphicsScene *_tabla;
    QVector<Zaposleni *> _zaposleni;
};
#endif // MAINWINDOW_HPP
