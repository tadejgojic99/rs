#pragma once 

#include <stdexcept>
#include <string>
#include "Student.hpp"

using namespace std;

class FilterStudenata
{
public:
    FilterStudenata(char uslov, double vrednost)
        : _uslov(proveriUslov(uslov))
        , _vrednost(vrednost)
    {
    }

    vector<string> operator()(const vector<Student> &studenti) const
    {
        if (studenti.empty())
        {
            return {};
        }

        vector<string> filtrirani;
        
        for (const auto &student : studenti)
        {
            filtrirajStudenta(student, filtrirani);
        }

        return filtrirani;
    }

private:
    char proveriUslov(char uslov)
    {
        if (uslov != 'l' && uslov != 'g' && uslov != 'e')
        {
            throw invalid_argument("uslov");
        }
        return uslov;
    }

    void filtrirajStudenta(const Student &student, vector<string> &filtrirani) const 
    {
        if (!ispunjavaUslov(student))
        {
            return;
        }

        const auto imePrezime = student.imePrezime();
        filtrirani.push_back(imePrezime);
    }

    bool ispunjavaUslov(const Student &student) const
    {
        if (_uslov == 'e')
        {
            return ispunjavaUslovJednakosti(student);
        }
        else if (_uslov == 'l')
        {
            return ispunjavaUslovManjeOd(student);
        }
        else if (_uslov == 'g')
        {
            return ispunjavaUslovVeceOd(student);
        }
        return false;
    }

    bool ispunjavaUslovJednakosti(const Student &student) const 
    {
        return student.prosecnaOcena() == _vrednost;
    }

    bool ispunjavaUslovManjeOd(const Student &student) const 
    {
        return student.prosecnaOcena() < _vrednost;
    }

    bool ispunjavaUslovVeceOd(const Student &student) const 
    {
        return student.prosecnaOcena() > _vrednost;
    }

    char _uslov;
    double _vrednost;
};
