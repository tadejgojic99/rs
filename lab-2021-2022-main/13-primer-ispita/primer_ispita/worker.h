#ifndef WORKER_H
#define WORKER_H

#include <QThread>
#include <QVector>
#include <QMutex>

class Source;

class Worker : public QThread
{
    Q_OBJECT
public:
    Worker(const QVector<Source *> &sources, QMutex* mutex);

    // QThread interface
protected:
    void run() override;
private:
    QVector<Source*> m_sources;
    QMutex *m_mutex;
signals:
    void released(unsigned, unsigned);
};

#endif // WORKER_H
