#ifndef RECIPETHREAD_H
#define RECIPETHREAD_H

#include "recipe.h"
#include <QThread>
#include <QRandomGenerator>
#include <QMutex>

class RecipeThread : public QThread
{
    Q_OBJECT
public:
    RecipeThread(QVector<Recipe *> recipes, unsigned int id, QMutex *mutex, QObject *parent = nullptr);

signals:
    void buy(unsigned int i, unsigned int j);

protected:
    void run() override;

private:
    QVector<Recipe *> m_recipes;
    unsigned int m_id;
    QMutex *m_mutex;
};

#endif // RECIPETHREAD_H
