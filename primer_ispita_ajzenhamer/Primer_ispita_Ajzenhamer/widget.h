#ifndef WIDGET_H
#define WIDGET_H

#include "sources.h"
#include "sourcethread.h"
#include <QWidget>
#include <QFileDialog>
#include <QXmlStreamReader>
#include <QMutex>
#include <QMutexLocker>

QT_BEGIN_NAMESPACE
namespace Ui { class Widget; }
QT_END_NAMESPACE

class Widget : public QWidget
{
    Q_OBJECT

public:
    Widget(QWidget *parent = nullptr);
    ~Widget();

private:
    Ui::Widget *ui;
    QMutex m_mutex;
    bool deserializeSources();
    void showSources();
    void showTotalVolume();
    QVector<Sources *> m_sources;

public slots:
    void fillSources();
    void startTransfer();
    void sourceLostFluid(unsigned int i, unsigned int fluidLost);

};
#endif // WIDGET_H
