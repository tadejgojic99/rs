#ifndef CITY_H
#define CITY_H

#include <QVariant>
#include <QString>
#include <math.h>

class City
{
public:
    City(const QString &name = "", const double &x = 0.0, const double &y = 0.0);
    City(const QVariant &variant);

    void fromQVariant(const QVariant &variant);
    QString toQString();

    bool visited();
    void visit();

    double distance(const City &other) const;
    QString name() const;
private:
    QString m_name;
    double m_x;
    double m_y;
    bool m_visited;
};

#endif // CITY_H
