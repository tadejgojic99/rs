#pragma once

namespace matf
{
    // Ponekad implementacija metoda koji dolaze u parovima
    // i zavise jedan od drugog mogu da nam dosade.
    // Na primer, zasto ne napravimo klasu koja ce nam
    // dozvoljavati da definisemo samo operator ==,
    // a != da bude automatski implementiran (ili obrnuto)?
    template <typename T>
    class Poredjenje
    {
    public:
        bool operator==(const T &other) const
        {
            // Dva objekta su jednaka ako nisu razlicita
            return !(static_cast<const T &>(*this) != other);
        }

        bool operator!=(const T &other) const
        {
            // Dva objekta su razlicita ako nisu jednaka
            return !(static_cast<const T &>(*this) == other);
        }
    };
}