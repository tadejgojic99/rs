#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "citythread.h"
#include "city.h"
#include <QMainWindow>
#include <QJsonDocument>
#include <QFile>
#include <QFileDialog>
#include <QVector>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

public slots:
    void on_ucitajGradove_pushButton_clicked();
    void on_putevi_pushButton_clicked();
    void onBuildRoad(int currId, int nextId);

private:
    Ui::MainWindow *ui;
    QVector<City *> m_cities;
    int m_count;
    void startThread(int id);
};
#endif // MAINWINDOW_H
