#pragma once

#include <iostream>
#include <string>
#include <numeric>
#include <vector>
#include <iterator>

using namespace std;

template <typename T>
bool manjiIliJednaki(const T &levi, const T &desni)
{
    return levi <= desni;
}

template <typename Kolekcija,
          typename Element = typename Kolekcija::value_type>
bool proveriUredjenost(const Kolekcija &xs)
{
    return inner_product(
        cbegin(xs),
        cend(xs) - 1,
        cbegin(xs) + 1,
        true,
        [](const auto &akumulator, const auto &jednaki)
        { return akumulator && jednaki; },
        manjiIliJednaki<Element>);
}