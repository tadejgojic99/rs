#include "MainWindow.hpp"
#include "ui_MainWindow.h"
#include "Tabla.hpp"
#include "Zaposleni.hpp"
#include "CvorZaposleni.hpp"

MainWindow::MainWindow(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::MainWindow)
    , _tabla(new Tabla(this))
{
    ui->setupUi(this);

    // Povezujemo scenu i pogled
    _tabla->setSceneRect(ui->gvTabla->rect());
    ui->gvTabla->setScene(_tabla);
    ui->gvTabla->setRenderHint(QPainter::Antialiasing);
    ui->gvTabla->setAlignment(Qt::AlignTop | Qt::AlignLeft);

    // Podesavamo polja u listi za izbor pozicije zaposlenih
    ui->cbPozicija->addItem("Sefovi");
    ui->cbPozicija->addItem("Radnici");

    // Signali i slotovi
    connect(ui->pbDodajNaTablu, &QPushButton::clicked, this, &MainWindow::DodajNovogZaposlenog);
    connect(this, &MainWindow::DodatNoviRadnik, dynamic_cast<Tabla *>(_tabla), &Tabla::DodatZaposleniNaTablu);
}

MainWindow::~MainWindow()
{
    delete ui;
    for (auto zaposleni : _zaposleni)
    {
        delete zaposleni;
    }
}

void MainWindow::DodajNovogZaposlenog()
{
    const auto imePrezime = ui->leImePrezime->text();
    const auto pozicija = static_cast<Zaposleni::Pozicije>(ui->cbPozicija->currentIndex());
    const auto zaposleni = new Zaposleni(imePrezime, pozicija);

    _zaposleni.push_back(zaposleni);

    const auto cvor = new CvorZaposleni(zaposleni);
    _tabla->addItem(cvor);

    emit DodatNoviRadnik(cvor);
}

