#include <iostream>
#include <vector>

namespace matf
{
    // Funkcija `najveciElement` vraca iterator na maksimalni element.
    // Elementi se porede koristeci operator `<`.
    template <typename ForwardIterator>
    ForwardIterator najveciElement(ForwardIterator first, ForwardIterator last)
    {
        // Prazan interval.
        if (first == last)
        {
            return last;
        }

        // Pretpostavimo da je prvi najveci.
        ForwardIterator largest = first;
        ++first;
        for (; first != last; ++first)
        {
            if (*largest < *first)
            {
                largest = first;
            }
        }
        return largest;
    }
}

int main()
{
    std::vector<int> xs{5, 9, -7, 18, -25, 0, 9, 25, 18, 19};
    std::cout << "Najveci element brojeva: " << *matf::najveciElement(xs.cbegin(), xs.cend()) << std::endl;

    std::vector<std::string> ys{"C++", "C#", "Java", "Rust", "TypeScript"};
    std::cout << "Najveci element niski: " << *matf::najveciElement(ys.cbegin(), ys.cend()) << std::endl;

    return 0;
}
