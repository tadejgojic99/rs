#include <QPainter>
#include "CvorZaposleni.hpp"
#include "Zaposleni.hpp"

CvorZaposleni::CvorZaposleni(Zaposleni *zaposleni)
    : QGraphicsItem()
    , _zaposleni(zaposleni)
{

}

QRectF CvorZaposleni::boundingRect() const
{
    return QRectF(0, 0, Sirina(), Visina());
}

void CvorZaposleni::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    Q_UNUSED(option)
    Q_UNUSED(widget)

    if (_zaposleni->Pozicija() == Zaposleni::Pozicije::Sefovi) {
        painter->fillRect(boundingRect(), QColor::fromRgb(118, 83, 219));
        painter->setPen(Qt::white);
    }
    else if (_zaposleni->Pozicija() == Zaposleni::Pozicije::Radnici) {
        painter->fillRect(boundingRect(), QColor::fromRgb(48, 133, 242));
        painter->setPen(Qt::black);
    }

    const auto tekst = QString("%1\n%2").arg(_zaposleni->ImePrezime(), _zaposleni->NazivPozicije());
    painter->drawText(boundingRect(), Qt::AlignHCenter | Qt::AlignVCenter, tekst);
}

QPointF CvorZaposleni::PozicijaCentraVrha() const
{
    return pos() + QPointF(Sirina() / 2, 0);
}

QPointF CvorZaposleni::PozicijaCentraDna() const
{
    return pos() + QPointF(Sirina() / 2, Visina());
}
