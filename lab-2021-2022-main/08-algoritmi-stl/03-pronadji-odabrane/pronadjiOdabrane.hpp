#pragma once

#include <vector>
#include <iostream>
#include <algorithm>
#include <iterator>
#include <string>

using namespace std;

vector<bool> pronadjiOdabrane(const vector<string> &stavke)
{
    vector<bool> indikatori;

    transform(cbegin(stavke), cend(stavke), back_inserter(indikatori), [](const auto &stavka)
              { return !stavka.empty() && stavka[0] == '*'; });

    return indikatori;
}