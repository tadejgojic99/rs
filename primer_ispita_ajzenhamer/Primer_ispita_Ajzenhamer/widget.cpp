#include "widget.h"
#include "ui_widget.h"

Widget::Widget(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::Widget)
{
    ui->setupUi(this);

    QObject::connect(ui->pbFillSources, &QPushButton::clicked,
                     this, &Widget::fillSources);

    QObject::connect(ui->pbStartTransfer, &QPushButton::clicked,
                     this, &Widget::startTransfer);
}

Widget::~Widget()
{
    delete ui;
    qDeleteAll(m_sources);
}

void Widget::fillSources()
{
    bool ind = deserializeSources();
    if(!ind) {
        return;
    }
    showSources();
    showTotalVolume();
}

bool Widget::deserializeSources()
{
    const auto fileName = QFileDialog::getOpenFileName(this, "Select XML file with sources", "../", "XML files(*.xml)");
    QFile input(fileName);
    if(!input.open(QFile::ReadOnly))
    {
        return false;
    }

    qDeleteAll(m_sources);
    m_sources.clear();

    QXmlStreamReader xmlReader(&input);
    xmlReader.readNextStartElement();  // <sources>

    while(xmlReader.readNextStartElement()) // <source>
    {
        auto source = new Sources();    // Sources() je u sustini jedan Source(), ali me mrzi da preimenujem
        QVariantMap map;

        while(xmlReader.readNextStartElement())   //  <name> i <volume>
        {
            const auto elementName = xmlReader.name().toString();
            const auto elementValue = xmlReader.readElementText();

            if(elementName == "name")
            {
                map.insert("name", elementValue);
            }
            else if(elementName == "volume")
            {
                map.insert("volume", elementValue.toUInt());
            }
        }

        source->fromQVariant(map);
        m_sources.push_back(source);
    }

    return true;
}

void Widget::showSources()
{
    ui->listWidget->clear();

    for(const auto source : m_sources)
    {
        ui->listWidget->addItem(source->toQString());
    }
}

void Widget::showTotalVolume()
{
    auto totalVolume = 0;
    for(const auto source : m_sources)
    {
        totalVolume += source->getVolume();
    }

    ui->leTotalSourceVolume->setText(QString::number(totalVolume));
}

void Widget::startTransfer()
{
    unsigned int totalReservoirVolume = ui->leTotalRezevoarVolume->text().toUInt();
    Q_UNUSED(totalReservoirVolume);
    ui->leCurrentRezervoarVolume->setText(QString::number(0u));
    ui->leLoss->setText(QString::number(0u));

    for(const auto &source : m_sources)
    {
        Q_UNUSED(source);

        auto thread = new SourceThread(m_sources, &m_mutex);
        connect(thread, &SourceThread::sourceLostFluid, this, &Widget::sourceLostFluid);
        connect(thread, &SourceThread::finished, thread, &SourceThread::deleteLater);

        thread->start();
    }


}

void Widget::sourceLostFluid(unsigned int i, unsigned int fluidLost)
{
    QMutexLocker lock(&m_mutex);

    ui->listWidget->item(i)->setText(m_sources[i]->toQString());
    const unsigned int fullTank = ui->leTotalRezevoarVolume->text().toUInt();
    unsigned int currTank = ui->leCurrentRezervoarVolume->text().toUInt();
    unsigned int currLoss = ui->leLoss->text().toUInt();

    if(currTank + fluidLost <= fullTank)
    {
        currTank += fluidLost;
        ui->leCurrentRezervoarVolume->setText(QString::number(currTank));
    }
    else
    {
        unsigned int loss = currTank + fluidLost - fullTank;
        currLoss += loss;
        currTank = fullTank;
        ui->leLoss->setText(QString::number(currLoss));
        ui->leCurrentRezervoarVolume->setText(QString::number(currTank));
    }

}
