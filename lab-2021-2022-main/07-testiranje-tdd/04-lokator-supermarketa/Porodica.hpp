#pragma once

#include <cmath>

using namespace std;

class Porodica
{
public:
    Porodica(double x, double y)
        : _x(x), _y(y)
    {
    }

    bool operator==(const Porodica &p) const
    {
        return _x == p._x && _y == p._y;
    }

    bool operator<(const Porodica &p) const
    {
        return _x < p._x && _y < p._y;
    }

    double rastojanjeDo(double x, double y) const
    {
        return sqrt(pow(_x - x, 2) + pow(_y - y, 2));
    }

    bool ziviUKvadrantu(unsigned kvadrant) const
    {
        if (kvadrant == 1u)
        {
            return signbit(_x) == signbit(_y) && _x + _y >= 0.0;
        }
        if (kvadrant == 2u)
        {
            return signbit(_x) != signbit(_y) && _x <= 0.0 && _y >= 0.0;
        }
        if (kvadrant == 3u)
        {
            return signbit(_x) == signbit(_y) && _x + _y <= 0.0;
        }
        return signbit(_x) == signbit(_y) && _x >= 0.0 && _y <= 0.0;
    }

private:
    double _x;
    double _y;
};