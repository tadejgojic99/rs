#include "widget.h"
#include "ui_widget.h"
#include "town.h"
#include "solver.h"

Widget::Widget(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::Widget)
{
    ui->setupUi(this);

    _town = new Town(5);
    populateTable();
}

Widget::~Widget()
{
    delete ui;
}

Town& Widget::getTown()
{
    return *_town;
}
QMutex & Widget::getMutexForTown()
{
    return _mutexForTown;
}

void Widget::on_zapocniDonacije_pushButton_clicked()
{
    startThreads();
}

void Widget::populateTable()
{
    const auto rcNumber = _town->numberOfRowsAndColumns();

    ui->tableWidget->setRowCount(rcNumber);
    ui->tableWidget->setColumnCount(rcNumber);
    for (auto i = 0; i < rcNumber; ++i)
    {
        for (auto j = 0; j < rcNumber; ++j)
        {
            auto cellValue = _town->cellValue(i, j);
            // QTableWidgetItem-i se kreiraju bez roditelja,
            // ali kad se dodaju nekom QTableWidget-u, onda on preuzima vlasnistvo nad njima
            auto tableItem = new QTableWidgetItem(QString::number(cellValue));
            ui->tableWidget->setItem(i, j, tableItem);
        }
    }
}

void Widget::startThreads()
{
    // Ovde pristupamo objektu sobe za citanje broja kolona,
    // medjutim, moguce je da neka nit jos uvek radi,
    // pa imamo konkurentno citanje, te moramo sinhronizovati niti.
    QMutexLocker lock(&_mutexForTown);

    for (auto i = 0; i < _town->numberOfRowsAndColumns(); ++i)
    {
        auto thread = new Solver(i, this);
        QObject::connect(thread, &Solver::threadFinished,
                         this, &Widget::onThreadFinished);
        QObject::connect(thread, &Solver::finished,
                         thread, &Solver::deleteLater);
        thread->start();
    }
}

void Widget::onThreadFinished(int i, int val)
{
    QMutexLocker lock(&_mutexForTown);
    bool ind = false;
    for(int j = 0; j < ui->tableWidget->rowCount(); ++j)
    {
        auto cellValue = _town->cellValue(i, j);
        if(cellValue < 0)
        {
            ind = true;
        }

        ui->tableWidget->item(i, j)->setText(QString::number(cellValue));
    }

    ui->listWidget->addItem("Red: " + QString::number(i) + " | vrednost: " + QString::number(val));

    if(ind)
    {
        auto thread = new Solver(i, this);
        QObject::connect(thread, &Solver::threadFinished,
                         this, &Widget::onThreadFinished);
        QObject::connect(thread, &Solver::finished,
                         thread, &Solver::deleteLater);
        thread->start();
    }

}
