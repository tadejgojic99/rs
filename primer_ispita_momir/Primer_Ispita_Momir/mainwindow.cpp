#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QDebug>
MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow), m_count(0)
{
    ui->setupUi(this);

//    QObject::connect(ui->ucitajGradove_pushButton, &QPushButton::clicked,
//                     this, &MainWindow::on_ucitajGradove_pushButton_clicked);
//    QObject::connect(ui->putevi_pushButton, &QPushButton::clicked,
//                     this, &MainWindow::on_izgradiPuteve_pushButton_clicked);
}

MainWindow::~MainWindow()
{
    delete ui;
    qDeleteAll(m_cities);
}

void MainWindow::on_ucitajGradove_pushButton_clicked()
{
    const QString path = QFileDialog::getOpenFileName(this, "Otvori JSON", "../", "JSON files(*.json)");
    QFile file(path);
    file.open(QFile::ReadOnly);
    const auto document = QJsonDocument::fromJson(file.readAll());
    const auto variantData = document.toVariant();

    QVariantList list = variantData.toList();
    for(const auto &e : list)
    {
        City *city = new City();
        city->fromQVariant(e);

        m_cities.push_back(city);
    }

    for(City *city : m_cities)
    {
        ui->gradovi_textEdit->append(city->toQString());
    }

    ui->ucitajGradove_pushButton->setEnabled(false);
    file.close();
}

void MainWindow::on_putevi_pushButton_clicked()
{
    ui->putevi_pushButton->setEnabled(false);

    startThread(0);
}

void MainWindow::onBuildRoad(int currId, int nextId)
{
    m_count++;
    if(m_count < m_cities.size()) {
        ui->putevi_textEdit->append("Added a new road from " + m_cities.at(currId)->name() + " to " + m_cities.at(nextId)->name());
        startThread(nextId);
    }
    else
    {
        ui->putevi_textEdit->append("----------------------------\n----------------------------\nAll roads have been built!");
    }
}

void MainWindow::startThread(int id)
{
    auto thread = new CityThread(m_cities, id, this);

    QObject::connect(thread, &CityThread::buildRoad,
                     this, &MainWindow::onBuildRoad);
    QObject::connect(thread, &CityThread::finished,
                     thread, &CityThread::deleteLater);
    thread->start();
}
