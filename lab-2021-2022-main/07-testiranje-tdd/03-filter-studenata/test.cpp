#define CATCH_CONFIG_MAIN
#include "catch.hpp"

#include "Student.hpp"
#include "FilterStudenata.hpp"

TEST_CASE("FilterStudenata", "[class]")
{
    SECTION("Konstruktor prijavljuje invalid_argument izuzetak ako se prosledi karakter koji nije l, g ili e")
    {
        // Arrange
        const auto ulaz = 'x';

        // Act + Assert
        REQUIRE_THROWS_AS(FilterStudenata(ulaz, 0.0), invalid_argument);
    }

    SECTION("operator() vraca prazan vektor stringova ako se prosledi prazan vektor studenata")
    {
        // Arrange
        const FilterStudenata filter('e', 0.0);
        const vector<Student> ulaz;
        const vector<string> ocekivanIzlaz;

        // Act
        const auto dobijenIzlaz = filter(ulaz);
        
        // Assert
        REQUIRE(dobijenIzlaz == ocekivanIzlaz);
    }

    SECTION("operator() vraca vektor sa jednim imenom i prezimenom ako se prosledi vektor sa jednim studentom koji ispunjava uslov jednakosti")
    {
        // Arrange
        const FilterStudenata filter('e', 10.0);
        const vector<Student> ulaz{ Student("Pera", "Peric", 10.0) };
        const vector<string> ocekivanIzlaz{ "Pera Peric" };

        // Act
        const auto dobijenIzlaz = filter(ulaz);
        
        // Assert
        REQUIRE(dobijenIzlaz == ocekivanIzlaz);
    }

    SECTION("operator() vraca vektor imena i prezimena samo onih studenata koji ispunjavaju uslov jednakosti ako se prosledi vektor sa vise studenata")
    {
        // Arrange
        const FilterStudenata filter('e', 10.0);
        const vector<Student> ulaz{ Student("Pera", "Peric", 10.0), Student("Laza", "Lazic", 8.4), Student("Milica", "Jovanovic", 10.0), Student("Sonja", "Stojanovic", 10.0), Student("Ana", "Nikolic", 9.5) };
        const vector<string> ocekivanIzlaz{ "Pera Peric", "Milica Jovanovic", "Sonja Stojanovic" };

        // Act
        const auto dobijenIzlaz = filter(ulaz);
        
        // Assert
        CHECK(dobijenIzlaz.size() == ocekivanIzlaz.size());
        REQUIRE(dobijenIzlaz == ocekivanIzlaz);
    }

    SECTION("operator() vraca vektor imena i prezimena samo onih studenata koji ispunjavaju uslov \"manje od\" ako se prosledi vektor sa vise studenata")
    {
        // Arrange
        const FilterStudenata filter('l', 10.0);
        const vector<Student> ulaz{ Student("Pera", "Peric", 10.0), Student("Laza", "Lazic", 8.4), Student("Milica", "Jovanovic", 10.0), Student("Sonja", "Stojanovic", 10.0), Student("Ana", "Nikolic", 9.5) };
        const vector<string> ocekivanIzlaz{ "Laza Lazic", "Ana Nikolic" };

        // Act
        const auto dobijenIzlaz = filter(ulaz);
        
        // Assert
        CHECK(dobijenIzlaz.size() == ocekivanIzlaz.size());
        REQUIRE(dobijenIzlaz == ocekivanIzlaz);
    }

    SECTION("operator() vraca vektor imena i prezimena samo onih studenata koji ispunjavaju uslov \"vece od\" ako se prosledi vektor sa vise studenata")
    {
        // Arrange
        const FilterStudenata filter('g', 9.0);
        const vector<Student> ulaz{ Student("Pera", "Peric", 10.0), Student("Laza", "Lazic", 8.4), Student("Milica", "Jovanovic", 10.0), Student("Sonja", "Stojanovic", 10.0), Student("Ana", "Nikolic", 9.5) };
        const vector<string> ocekivanIzlaz{ "Pera Peric", "Milica Jovanovic", "Sonja Stojanovic", "Ana Nikolic" };

        // Act
        const auto dobijenIzlaz = filter(ulaz);
        
        // Assert
        CHECK(dobijenIzlaz.size() == ocekivanIzlaz.size());
        REQUIRE(dobijenIzlaz == ocekivanIzlaz);
    }
}
