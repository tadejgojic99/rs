#include "solver.h"
#include <QRandomGenerator>
#include <QDebug>

Solver::Solver(const int i, QObject *parent)
    : QThread(parent)
    , _i(i)
{
    _value = QRandomGenerator::global()->bounded(5, 15);
}

void Solver::run()
{
    msleep(QRandomGenerator::global()->bounded(1, 5) * 1000);
    Widget *window = qobject_cast<Widget *>(parent());

    // Zakljucavamo muteks kako bismo sprecili da druge niti konkurentno pristupaju podacima
    QMutexLocker lock(&window->getMutexForTown());

    // Sada mozemo bezbedno da pristupimo objektu sobe za citanje i menjanje.
    // Ponovo, obratite paznju da se cuva referenca da bi se izbeglo kopiranje.
    auto &town = window->getTown();

    // Racunamo razliku u temperaturi izmedju tekuce celije i okolnih celija

    // Dodajemo razliku u temperaturi od okolnih celija (konkurentno citanje)

    // Azuriramo temperaturu (konkurentno pisanje)
    for(int j = 0; j < town.numberOfRowsAndColumns(); ++j)
    {
        town.updateNewValueForCell(_i, j, _value);
    }

    // Emitujemo da smo zavrsili posao
    emit threadFinished(_i, _value);
}
