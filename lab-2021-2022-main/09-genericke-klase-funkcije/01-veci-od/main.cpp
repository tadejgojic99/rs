#include <iostream>

namespace matf
{
    // Sablonska funkcija koja vraca maksimum od dve prosledjene vrednosti.
    // Vrednosti mogu biti bilo kog tipa nad kojima je definisan operator '>'.
    // Kljucna rec `inline` sugerise kompilatoru da pokusa da
    // poziv funkcije zameni direktno sa implementacijom funkcije.
    // Time se moze izbeci cena pozivanja funkcije.
    template <typename T>
    inline T veciOd(T a, T b)
    {
        return a > b ? a : b;
    }
}

int main()
{
    std::cout << "matf::veciOd(2, 3) = " << matf::veciOd(2, 3) << std::endl;
    std::cout << "matf::veciOd(2.3, 2.4) = " << matf::veciOd(2.3, 2.4) << std::endl;
    // Poziv ispod ne prolazi zato sto se pokusava poziv funkcije matf::veciOd(int, double).
    // Medjutim, sablon koji smo napisali radi samo sa jednim tipom T.
    // std::cout << "matf::veciOd(2, 3.0) = " << matf::veciOd(2, 3.0) << std::endl;
    // Zbog toga je neophodno navesti koji tip zelimo da koristimo,
    // pa ce biti izvrsena implicitna konverzija vrednosti u taj tip
    // (ili kompilatorska greska, ukoliko to nije moguce).
    std::cout << "matf::veciOd(2, 3.0) = " << matf::veciOd<double>(2, 3.0) << std::endl;

    return 0;
}
