/********************************************************************************
** Form generated from reading UI file 'widget.ui'
**
** Created by: Qt User Interface Compiler version 5.15.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_WIDGET_H
#define UI_WIDGET_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Widget
{
public:
    QHBoxLayout *horizontalLayout;
    QVBoxLayout *verticalLayout;
    QPushButton *pbFillSources;
    QLabel *label;
    QListWidget *listWidget;
    QLabel *label_2;
    QLineEdit *leTotalSourceVolume;
    QVBoxLayout *verticalLayout_2;
    QPushButton *pbStartTransfer;
    QLabel *label_3;
    QLineEdit *leTotalRezevoarVolume;
    QLabel *label_4;
    QLineEdit *leCurrentRezervoarVolume;
    QLabel *label_5;
    QLineEdit *leLoss;
    QSpacerItem *verticalSpacer;

    void setupUi(QWidget *Widget)
    {
        if (Widget->objectName().isEmpty())
            Widget->setObjectName(QString::fromUtf8("Widget"));
        Widget->resize(800, 600);
        horizontalLayout = new QHBoxLayout(Widget);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        verticalLayout = new QVBoxLayout();
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        pbFillSources = new QPushButton(Widget);
        pbFillSources->setObjectName(QString::fromUtf8("pbFillSources"));

        verticalLayout->addWidget(pbFillSources);

        label = new QLabel(Widget);
        label->setObjectName(QString::fromUtf8("label"));
        label->setAlignment(Qt::AlignCenter);

        verticalLayout->addWidget(label);

        listWidget = new QListWidget(Widget);
        listWidget->setObjectName(QString::fromUtf8("listWidget"));

        verticalLayout->addWidget(listWidget);

        label_2 = new QLabel(Widget);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setAlignment(Qt::AlignCenter);

        verticalLayout->addWidget(label_2);

        leTotalSourceVolume = new QLineEdit(Widget);
        leTotalSourceVolume->setObjectName(QString::fromUtf8("leTotalSourceVolume"));
        leTotalSourceVolume->setEnabled(true);

        verticalLayout->addWidget(leTotalSourceVolume);


        horizontalLayout->addLayout(verticalLayout);

        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        pbStartTransfer = new QPushButton(Widget);
        pbStartTransfer->setObjectName(QString::fromUtf8("pbStartTransfer"));

        verticalLayout_2->addWidget(pbStartTransfer);

        label_3 = new QLabel(Widget);
        label_3->setObjectName(QString::fromUtf8("label_3"));
        label_3->setAlignment(Qt::AlignCenter);

        verticalLayout_2->addWidget(label_3);

        leTotalRezevoarVolume = new QLineEdit(Widget);
        leTotalRezevoarVolume->setObjectName(QString::fromUtf8("leTotalRezevoarVolume"));

        verticalLayout_2->addWidget(leTotalRezevoarVolume);

        label_4 = new QLabel(Widget);
        label_4->setObjectName(QString::fromUtf8("label_4"));
        label_4->setAlignment(Qt::AlignCenter);

        verticalLayout_2->addWidget(label_4);

        leCurrentRezervoarVolume = new QLineEdit(Widget);
        leCurrentRezervoarVolume->setObjectName(QString::fromUtf8("leCurrentRezervoarVolume"));
        leCurrentRezervoarVolume->setEnabled(false);

        verticalLayout_2->addWidget(leCurrentRezervoarVolume);

        label_5 = new QLabel(Widget);
        label_5->setObjectName(QString::fromUtf8("label_5"));
        label_5->setAlignment(Qt::AlignCenter);

        verticalLayout_2->addWidget(label_5);

        leLoss = new QLineEdit(Widget);
        leLoss->setObjectName(QString::fromUtf8("leLoss"));
        leLoss->setEnabled(false);

        verticalLayout_2->addWidget(leLoss);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_2->addItem(verticalSpacer);


        horizontalLayout->addLayout(verticalLayout_2);


        retranslateUi(Widget);

        QMetaObject::connectSlotsByName(Widget);
    } // setupUi

    void retranslateUi(QWidget *Widget)
    {
        Widget->setWindowTitle(QCoreApplication::translate("Widget", "Widget", nullptr));
        pbFillSources->setText(QCoreApplication::translate("Widget", "Popuni izvore", nullptr));
        label->setText(QCoreApplication::translate("Widget", "Izvori", nullptr));
        label_2->setText(QCoreApplication::translate("Widget", "Ukupna zapremina izvora", nullptr));
        pbStartTransfer->setText(QCoreApplication::translate("Widget", "Zapocni transfet", nullptr));
        label_3->setText(QCoreApplication::translate("Widget", "Ukupna zapremina rezervoara", nullptr));
        label_4->setText(QCoreApplication::translate("Widget", "Tekuca zapremina rezervoara", nullptr));
        label_5->setText(QCoreApplication::translate("Widget", "Gubitak", nullptr));
    } // retranslateUi

};

namespace Ui {
    class Widget: public Ui_Widget {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_WIDGET_H
