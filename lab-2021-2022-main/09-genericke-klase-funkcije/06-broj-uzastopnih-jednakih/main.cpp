#include <iostream>
#include "brojUzastopnihJednakih.hpp"

using namespace std;

int main()
{
    const std::string text = "Hooloovoo";

    std::cerr << text << ": " << brojUzastopnihJednakih(text) << std::endl;

    const std::vector<double> numbers{-1.0, 2.36, 65.4, 65.4, 65.4, -1.0, 0.0, 5.4};

    std::cerr << "numbers: " << brojUzastopnihJednakih(numbers) << std::endl;

    return 0;
}
