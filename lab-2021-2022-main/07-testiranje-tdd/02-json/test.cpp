#define CATCH_CONFIG_MAIN
#include "catch.hpp"

#include "json.h"

#include <vector>
#include <map>
#include <string>

TEST_CASE("json", "[function]")
{
    // Specijalni slucajevi

    SECTION("Ako je niz objekata prazan, ocekujemo da bude ispaljen izuzetak tipa invalid_argument")
    {
        // Arrange
        const vector<map<string, string>> ulaz;

        // Act + Assert
        REQUIRE_THROWS_AS(json(ulaz), invalid_argument);
    }

    SECTION("Ako niz objekata ima 1 prazan objekat, ocekujemo nisku sa JSON nizom koja ima 1 prazan objekat")
    {
        // Arrange
        const vector<map<string, string>> ulaz = {{}};
        const string ocekivanIzlaz = "[{}]";

        // Act
        const auto dobijenIzlaz = json(ulaz);

        // Assert
        REQUIRE(dobijenIzlaz == ocekivanIzlaz);
    }

    SECTION("Ako niz objekata ima vise praznih objekata, ocekujemo nisku sa JSON nizom koja ima isti broj praznih objekata odvojenih zapetom")
    {
        // Arrange
        const vector<map<string, string>> ulaz = {{}, {}, {}, {}, {}};
        const string ocekivanIzlaz = "[{},{},{},{},{}]";

        // Act
        const auto dobijenIzlaz = json(ulaz);

        // Assert
        REQUIRE(dobijenIzlaz == ocekivanIzlaz);
    }

    SECTION("Ako niz objekata ima 1 objekat sa 1 svojstvom, ocekujemo nisku sa JSON nizom koja ima 1 objekat sa 1 svojstvom")
    {
        // Arrange
        const vector<map<string, string>> ulaz = {{make_pair("x", "1")}};
        const string ocekivanIzlaz = "[{\"x\":\"1\"}]";

        // Act
        const auto dobijenIzlaz = json(ulaz);

        // Assert
        REQUIRE(dobijenIzlaz == ocekivanIzlaz);
    }

    // Uobicajeni slucajevi

    SECTION("Ako niz objekata ima 1 objekat sa vise svojstava, ocekujemo nisku sa JSON nizom koja ima 1 objekat sa vise svojstava")
    {
        // Arrange
        const vector<map<string, string>> ulaz = {{make_pair("a", "1"), make_pair("b", "2"), make_pair("c", "3")}};
        const string ocekivanIzlaz = "[{\"a\":\"1\",\"b\":\"2\",\"c\":\"3\"}]";

        // Act
        const auto dobijenIzlaz = json(ulaz);

        // Assert
        REQUIRE(dobijenIzlaz == ocekivanIzlaz);
    }

    SECTION("Ako niz objekata ima vise objekata sa vise svojstava, ocekujemo nisku sa JSON nizom koja ima vise objekata sa vise svojstava")
    {
        // Arrange
        const vector<map<string, string>> ulaz = {{make_pair("a", "1"), make_pair("b", "2"), make_pair("c", "3")}, {make_pair("a", "1"), make_pair("b", "2"), make_pair("c", "3")}};
        const string ocekivanIzlaz = "[{\"a\":\"1\",\"b\":\"2\",\"c\":\"3\"},{\"a\":\"1\",\"b\":\"2\",\"c\":\"3\"}]";

        // Act
        const auto dobijenIzlaz = json(ulaz);

        // Assert
        REQUIRE(dobijenIzlaz == ocekivanIzlaz);
    }
}
