#ifndef WIDGET_H
#define WIDGET_H

#include "recipe.h"
#include <QWidget>
#include <QString>
#include <QVector>
#include <QFileDialog>
#include <QFile>
#include <QJsonDocument>
#include <QMutex>

QT_BEGIN_NAMESPACE
namespace Ui { class Widget; }
QT_END_NAMESPACE

class Widget : public QWidget
{
    Q_OBJECT

public:
    Widget(QWidget *parent = nullptr);
    ~Widget();

public slots:
    void on_ucitaj_pushButton_clicked();
    void on_zapocni_pushButton_clicked();
    void onBuy(const unsigned int &i, const unsigned int &j);

private:
    Ui::Widget *ui;
    QVector<Recipe *> m_recipes;
    QMutex m_mutex;
};
#endif // WIDGET_H
