#include <string>
#include <vector>
#include <algorithm>
#include <numeric>
#include <iterator>
#include <iostream>

class Product
{
private:
    std::string _pno;
    double _price;

public:
    Product(std::string &&pno, double price)
        : _pno(std::move(pno)), _price(price)
    {
    }

    double Price() const
    {
        return _price;
    }
};

class Box
{
private:
    bool _opened;

public:
    Box(bool opened)
        : _opened(opened)
    {
    }
};

class GlassBox : public Box
{
public:
    GlassBox(bool opened)
        : Box(opened)
    {
    }
};

class ProductList
{
private:
    std::vector<Product> _products;

protected:
    ProductList(std::vector<Product> &&products)
        : _products(std::move(products))
    {
    }

public:
    void AddProduct(Product p)
    {
        _products.push_back(p);
    }

    double Price() const
    {
        return std:: transform_reduce(
            std::cbegin(_products),
            std::cend(_products),
            double{},
            std::plus<double>(),
            [](const auto &product)
            { return product.Price(); });
    }
};

// Virtualno nasledjivanje! Videti klasu WrappedCartonBox ispod.
class WrappingPaper : public virtual ProductList
{
private:
    std::string _patternNo;

public:
    WrappingPaper(std::vector<Product> &&products, std::string &&patternNo)
        : ProductList(std::move(products))
        , _patternNo(patternNo)
    {
    }
};

// Virtualno nasledjivanje! Videti klasu WrappedCartonBox ispod.
class CartonBox : public Box, public virtual ProductList
{
private:
    std::string _color;

public:
    // Redosled poziva konstruktora je vazan ovde, 
    // posto se prvo poziva konstruktor virtualno nasledjene klase 
    CartonBox(bool opened, std::vector<Product> &&products, std::string &&color = "neutral")
        : ProductList(std::move(products))
        , Box(opened)
        , _color(std::move(color))
    {
    }
};

// Posto ova klasa indirektno nasledjuje klasu ProductList preko CartonBox i WrappingPaper,
// potrebno je da u te dve klase virtualno naslede klasu ProductList.
// Dodatno, neophodno je da u ovoj klasi eksplicitno pozovemo konstruktor ProductList.
// Ukoliko to ne uradimo, bice pozvan podrazumevani konstruktor ProductList() ako postoji,
// a ako ne postoji, dobijamo gresku
class WrappedCartonBox : public CartonBox, public WrappingPaper
{
public:
    WrappedCartonBox(bool opened, std::vector<Product> &&products, std::string &&patternNo, std::string &&color = "neutral")
        : ProductList(std::move(products))
        , CartonBox(opened, std::move(products), std::move(color))
        , WrappingPaper(std::move(products), std::move(patternNo))
    {
    }
};

int main()
{
    WrappingPaper yellowPaper({Product("picture-frame-1", 5.99), Product("old-pen-4", 14.99)}, "yellow-1");
    std::cout << yellowPaper.Price() << std::endl;

    CartonBox blueCartonBox(false, {}, "blue");
    blueCartonBox.AddProduct(Product("old-shirt-with-flowers-1", 13.99));
    blueCartonBox.AddProduct(Product("denim-jeans-3", 26.99));
    std::cout << blueCartonBox.Price() << std::endl;

    WrappedCartonBox neutralWrappedCartonBox(
        false,
        {Product("picture-frame-1", 5.99),
         Product("old-pen-4", 14.99),
         Product("old-shirt-with-flowers-1", 13.99),
         Product("denim-jeans-3", 26.99)},
        "stripes");
    std::cout << neutralWrappedCartonBox.Price() << std::endl;

    return 0;
}
