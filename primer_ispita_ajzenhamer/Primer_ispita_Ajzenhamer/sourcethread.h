#ifndef SOURCETHREAD_H
#define SOURCETHREAD_H

#include "sources.h"
#include <QRandomGenerator>
#include <QThread>
#include <QMutex>
#include <QMutexLocker>

//class Sources;

class SourceThread : public QThread
{
    Q_OBJECT
public:
    SourceThread(QVector<Sources *> sources, QMutex *mutex, QObject *parent = nullptr);

protected:
    void run() override;

signals:
    void sourceLostFluid(unsigned int i, unsigned int fluidLost);

private:
    QVector<Sources *> m_sources;
    QMutex *m_mutex;
};

#endif // SOURCETHREAD_H
