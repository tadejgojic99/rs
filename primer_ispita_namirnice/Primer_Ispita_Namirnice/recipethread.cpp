#include "recipethread.h"

RecipeThread::RecipeThread(QVector<Recipe *> recipes, unsigned int id, QMutex *mutex, QObject *parent) : QThread(parent), m_recipes(recipes), m_id(id), m_mutex(mutex)
{

}

void RecipeThread::run()
{
    unsigned int repeatTimes = (QRandomGenerator::global()->generate() % 20) + 1;
    while(repeatTimes)
    {

        msleep(1000);

        QMutexLocker lock(m_mutex);
        unsigned int rand_i = QRandomGenerator::global()->generate() % m_recipes.size();
        unsigned int rand_j = QRandomGenerator::global()->generate() % m_recipes.at(rand_i)->list().size();

        emit buy(rand_i, rand_j);
        --repeatTimes;
    }
}
