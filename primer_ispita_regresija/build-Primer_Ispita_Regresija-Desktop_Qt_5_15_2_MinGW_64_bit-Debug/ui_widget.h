/********************************************************************************
** Form generated from reading UI file 'widget.ui'
**
** Created by: Qt User Interface Compiler version 5.15.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_WIDGET_H
#define UI_WIDGET_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Widget
{
public:
    QGridLayout *gridLayout;
    QVBoxLayout *verticalLayout_2;
    QHBoxLayout *horizontalLayout;
    QLabel *min_label;
    QLineEdit *min_lineEdit;
    QHBoxLayout *horizontalLayout_2;
    QLabel *max_label;
    QLineEdit *max_lineEdit;
    QHBoxLayout *horizontalLayout_3;
    QLabel *increment_label;
    QLineEdit *increment_lineEdit;
    QHBoxLayout *horizontalLayout_4;
    QPushButton *optimizuj_pushButton;
    QHBoxLayout *horizontalLayout_5;
    QLabel *resenje_label;
    QLineEdit *resenje_lineEdit;
    QHBoxLayout *horizontalLayout_18;
    QLabel *greska_label;
    QLineEdit *greska_lineEdit;

    void setupUi(QWidget *Widget)
    {
        if (Widget->objectName().isEmpty())
            Widget->setObjectName(QString::fromUtf8("Widget"));
        Widget->resize(886, 394);
        gridLayout = new QGridLayout(Widget);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        verticalLayout_2->setSizeConstraint(QLayout::SetNoConstraint);
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        min_label = new QLabel(Widget);
        min_label->setObjectName(QString::fromUtf8("min_label"));
        QFont font;
        font.setPointSize(16);
        min_label->setFont(font);

        horizontalLayout->addWidget(min_label);

        min_lineEdit = new QLineEdit(Widget);
        min_lineEdit->setObjectName(QString::fromUtf8("min_lineEdit"));

        horizontalLayout->addWidget(min_lineEdit);


        verticalLayout_2->addLayout(horizontalLayout);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        max_label = new QLabel(Widget);
        max_label->setObjectName(QString::fromUtf8("max_label"));
        max_label->setFont(font);

        horizontalLayout_2->addWidget(max_label);

        max_lineEdit = new QLineEdit(Widget);
        max_lineEdit->setObjectName(QString::fromUtf8("max_lineEdit"));

        horizontalLayout_2->addWidget(max_lineEdit);


        verticalLayout_2->addLayout(horizontalLayout_2);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        increment_label = new QLabel(Widget);
        increment_label->setObjectName(QString::fromUtf8("increment_label"));
        increment_label->setFont(font);

        horizontalLayout_3->addWidget(increment_label);

        increment_lineEdit = new QLineEdit(Widget);
        increment_lineEdit->setObjectName(QString::fromUtf8("increment_lineEdit"));

        horizontalLayout_3->addWidget(increment_lineEdit);


        verticalLayout_2->addLayout(horizontalLayout_3);

        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setObjectName(QString::fromUtf8("horizontalLayout_4"));
        optimizuj_pushButton = new QPushButton(Widget);
        optimizuj_pushButton->setObjectName(QString::fromUtf8("optimizuj_pushButton"));
        QSizePolicy sizePolicy(QSizePolicy::Minimum, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(5);
        sizePolicy.setHeightForWidth(optimizuj_pushButton->sizePolicy().hasHeightForWidth());
        optimizuj_pushButton->setSizePolicy(sizePolicy);
        QPalette palette;
        QBrush brush(QColor(58, 203, 172, 255));
        brush.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::Button, brush);
        QBrush brush1(QColor(255, 255, 0, 255));
        brush1.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::Midlight, brush1);
        QBrush brush2(QColor(255, 255, 255, 255));
        brush2.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::Text, brush2);
        QBrush brush3(QColor(240, 240, 240, 255));
        brush3.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Inactive, QPalette::Button, brush3);
        QBrush brush4(QColor(227, 227, 227, 255));
        brush4.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Inactive, QPalette::Midlight, brush4);
        QBrush brush5(QColor(0, 0, 0, 255));
        brush5.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Inactive, QPalette::Text, brush5);
        palette.setBrush(QPalette::Disabled, QPalette::Button, brush);
        palette.setBrush(QPalette::Disabled, QPalette::Midlight, brush1);
        QBrush brush6(QColor(120, 120, 120, 255));
        brush6.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Disabled, QPalette::Text, brush6);
        optimizuj_pushButton->setPalette(palette);
        QFont font1;
        font1.setPointSize(14);
        optimizuj_pushButton->setFont(font1);

        horizontalLayout_4->addWidget(optimizuj_pushButton);


        verticalLayout_2->addLayout(horizontalLayout_4);

        horizontalLayout_5 = new QHBoxLayout();
        horizontalLayout_5->setObjectName(QString::fromUtf8("horizontalLayout_5"));
        resenje_label = new QLabel(Widget);
        resenje_label->setObjectName(QString::fromUtf8("resenje_label"));
        resenje_label->setFont(font);

        horizontalLayout_5->addWidget(resenje_label);

        resenje_lineEdit = new QLineEdit(Widget);
        resenje_lineEdit->setObjectName(QString::fromUtf8("resenje_lineEdit"));

        horizontalLayout_5->addWidget(resenje_lineEdit);


        verticalLayout_2->addLayout(horizontalLayout_5);

        horizontalLayout_18 = new QHBoxLayout();
        horizontalLayout_18->setObjectName(QString::fromUtf8("horizontalLayout_18"));
        greska_label = new QLabel(Widget);
        greska_label->setObjectName(QString::fromUtf8("greska_label"));
        greska_label->setFont(font);

        horizontalLayout_18->addWidget(greska_label);

        greska_lineEdit = new QLineEdit(Widget);
        greska_lineEdit->setObjectName(QString::fromUtf8("greska_lineEdit"));

        horizontalLayout_18->addWidget(greska_lineEdit);


        verticalLayout_2->addLayout(horizontalLayout_18);


        gridLayout->addLayout(verticalLayout_2, 0, 0, 1, 1);


        retranslateUi(Widget);

        QMetaObject::connectSlotsByName(Widget);
    } // setupUi

    void retranslateUi(QWidget *Widget)
    {
        Widget->setWindowTitle(QCoreApplication::translate("Widget", "Widget", nullptr));
        min_label->setText(QCoreApplication::translate("Widget", "min", nullptr));
        max_label->setText(QCoreApplication::translate("Widget", "max", nullptr));
        increment_label->setText(QCoreApplication::translate("Widget", "increment", nullptr));
        optimizuj_pushButton->setText(QCoreApplication::translate("Widget", "Optimizuj", nullptr));
        resenje_label->setText(QCoreApplication::translate("Widget", "resenje", nullptr));
        greska_label->setText(QCoreApplication::translate("Widget", "greska", nullptr));
    } // retranslateUi

};

namespace Ui {
    class Widget: public Ui_Widget {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_WIDGET_H
