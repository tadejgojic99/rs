#include "solver.h"

Solver::Solver(double min, double max, double increment, std::vector<double> xs, std::vector<double> ys)
    : m_min(min), m_max(max), m_increment(increment), m_xs(xs), m_ys(ys)
{

}

void Solver::run()
{
    double minGreska = static_cast<double>(INFINITY);
//    int n = static_cast<int>(m_xs.size());

    for(double min = m_min; min < m_max; min += m_increment)
    {
        for(double max = min; max <= m_max; max += m_increment)
        {
            double a1 = min;
            double b1 = max;
            LinearRegression lr1(a1, b1);

            double a2 = max;
            double b2 = min;
            LinearRegression lr2(a2, b2);

            double greska1 = lr1.error(m_xs, m_ys);
            double greska2 = lr2.error(m_xs, m_ys);
            if(greska1 < minGreska)
            {
                emit update(greska1, lr1.toQString());
                minGreska = greska1;
            }
            if(greska2 < minGreska)
            {
                emit update(greska2, lr2.toQString());
                minGreska = greska2;
            }
        }
    }
//    double a = 1;
//    double b = 2;
//    LinearRegression lr(a, b);
//    double greska = lr.error(m_xs, m_ys);
//    emit update(greska, lr.toQString());
}
