#ifndef SOLVER_H
#define SOLVER_H

#include "widget.h"
#include <QThread>

class Solver : public QThread
{
    Q_OBJECT
public:
    Solver(const int i, QObject *parent = nullptr);

protected:
    void run() override;

signals:
    void threadFinished(int i, int val);

private:
    int _i;
    int _value;
};

#endif // SOLVER_H
