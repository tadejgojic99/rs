#include "widget.h"
#include "ui_widget.h"
#include "sources.h"
#include "worker.h"

#include<QFileDialog>
#include<QXmlStreamWriter>
#include<QListWidget>

Widget::Widget(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::Widget)
{
    ui->setupUi(this);
    connect(
                ui->pbFillSources, &QPushButton::clicked,
                this, &Widget::fillSources
    );
    connect(
                ui->pbStartTransfer, &QPushButton::clicked,
                this, &Widget::startTransfer
    );
}

Widget::~Widget()
{
    delete ui;
}

void Widget::fillSources()
{
    // deserijalizujem
    const QString path = QFileDialog::getOpenFileName(
                this,
                "Otvori XML",
                "",
                "XML Files(*.xml)"
                );

    QFile file(path);
    file.open(QFile::ReadOnly);
    QXmlStreamReader xmlStream(&file);

    qDeleteAll(m_sources);
    m_sources.clear();

    unsigned totalVolume = 0u;

    xmlStream.readNextStartElement();
    while(xmlStream.readNextStartElement()){
        QVariantMap map;
        xmlStream.readNextStartElement();
        auto volume = xmlStream.readElementText().toUInt();
        map.insert("volume", volume);
        totalVolume += volume;
        xmlStream.readNextStartElement();
        map.insert("name", xmlStream.readElementText());
        auto source = new Source(map);
        m_sources.push_back(source);

        auto lineItem = new QListWidgetItem(ui->listWidget);
        lineItem->setText(source->toQString());
        ui->listWidget->addItem(lineItem);
        xmlStream.readNextStartElement();
    }
    file.close();

    ui->leTotalSourceVolume->setText(QString::number(totalVolume));

}

void Widget::startTransfer()
{
    ui->leCurrentRezervoarVolume->setText("0");
    ui->leLoss->setText("0");

    for(auto source : m_sources){
        auto thread = new Worker(m_sources, &m_mutex);
        connect(
                    thread, &Worker::released,
                    this, &Widget::released
        );
        thread->start();
    }
}

void Widget::released(unsigned i, unsigned released_volume)
{
    QMutexLocker lock(&m_mutex);
    ui->listWidget->item(i)->setText(m_sources[i]->toQString());

    auto total = ui->leTotalRezevoarVolume->text().toUInt();
    auto current = ui->leCurrentRezervoarVolume->text().toUInt();

    if(current + released_volume <= total){
        ui->leCurrentRezervoarVolume->setText(QString::number(current + released_volume));
    }
    else{
        auto loss = ui->leLoss->text().toUInt();
        ui->leCurrentRezervoarVolume->setText(QString::number(total));
        loss += current + released_volume - total;
        ui->leLoss->setText(QString::number(loss));
    }
}

