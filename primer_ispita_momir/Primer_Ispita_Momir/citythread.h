#ifndef CITYTHREAD_H
#define CITYTHREAD_H

#include "city.h"
#include <QThread>
#include <QVector>

class CityThread : public QThread
{
    Q_OBJECT
public:
    CityThread(QVector<City *> cities, unsigned int id, QObject *parent = nullptr);

signals:
    void buildRoad(int currId, int nextId);

protected:
    void run() override;

private:
    void trace(const char *msg) const;

    QVector<City *> m_cities;
    unsigned int m_id;
};

#endif // CITYTHREAD_H
