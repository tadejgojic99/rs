#define CATCH_CONFIG_MAIN
#include "catch.hpp"

#include "NGramKalkulator.hpp"

#include <string>
#include <algorithm>
#include <iterator>

TEST_CASE("NGram", "[class]")
{
    SECTION("Kada se prosledi prazna niska, konstruktor prijavljuje izuzetak")
    {
        // Arrange
        const string tekst;

        // Act + Assert
        REQUIRE_THROWS(NGramKalkulator(tekst));
    }

    SECTION("Kada se prosledi duzina ngrama koja je veca od teksta, operator() prijavljuje izuzetak")
    {
        // Arrange
        const string tekst{"abc"};
        const NGramKalkulator kalkulator(tekst);
        const auto ulaz = static_cast<unsigned>(tekst.size() + 1u);

        // Act + Assert
        REQUIRE_THROWS(kalkulator(ulaz));
    }

    SECTION("Kada se prosledi duzina ngrama koja je najvise duzine teksta, operator() vraca vektor gde su svi ngrami prosledjene duzine")
    {
        // Arrange
        const string tekst{"abbbabbaabba"};
        const NGramKalkulator kalkulator(tekst);
        const auto duzina = 3u;

        // Act
        const auto rezultat = kalkulator(duzina);

        // Assert
        REQUIRE(all_of(cbegin(rezultat), cend(rezultat), [&](const auto& ngram)
        {
            return ngram.tekst().size() == duzina;
        }));
    }

    SECTION("Kada se prosledi duzina ngrama koja je najvise duzine teksta, operator() vraca vektor ispravnih ngrama za taj tekst")
    {
        // Arrange
        const string tekst{"abbbabbaabba"};
        const NGramKalkulator kalkulator(tekst);
        const auto duzina = 3u;
        const auto ocekivanRezultat = vector<NGram>{ {"abb", 3u}, {"bbb", 1u}, {"bba", 3}, {"bab", 1u}, {"baa", 1u}, {"aab", 1u} };

        // Act
        const auto rezultat = kalkulator(duzina);

        // Assert
        REQUIRE(rezultat == ocekivanRezultat);
    }
}
