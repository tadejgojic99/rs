#ifndef CVORZAPOSLENI_HPP
#define CVORZAPOSLENI_HPP

#include <QGraphicsObject>

class Zaposleni;

class CvorZaposleni : public QGraphicsObject
{
    Q_OBJECT

public:
    CvorZaposleni(Zaposleni *zaposleni);

    QRectF boundingRect() const override;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) override;

    inline const Zaposleni *Zaposlen() const
    {
        return _zaposleni;
    }

    QPointF PozicijaCentraVrha() const;
    QPointF PozicijaCentraDna() const;

    inline qint32 Sirina() const { return 150; }
    inline qint32 Visina() const { return 50; }

signals:
    void Pomeren();

protected:
    void mouseMoveEvent(QGraphicsSceneMouseEvent *event) override;

private:
    Zaposleni *_zaposleni;
};

#endif // CVORZAPOSLENI_HPP
