#include "widget.h"
#include "ui_widget.h"
#include "solver.h"

Widget::Widget(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::Widget)
    , m_xs({0, 1}), m_ys({1, 2})
{
    ui->setupUi(this);
}

Widget::~Widget()
{
    delete ui;
}

void Widget::onUpdate(double greska, QString string)
{
    ui->resenje_lineEdit->setText(string);
    ui->greska_lineEdit->setText(QString::number(greska));
}

void Widget::onFinish()
{
    ui->optimizuj_pushButton->setEnabled(true);
}

void Widget::on_optimizuj_pushButton_clicked()
{
    ui->optimizuj_pushButton->setEnabled(false);

    double min = ui->min_lineEdit->text().toDouble();
    double max = ui->max_lineEdit->text().toDouble();
    double increment = ui->increment_lineEdit->text().toDouble();

    auto thread = new Solver(min, max, increment, m_xs, m_ys);
    QObject::connect(thread, &Solver::update,
                     this, &Widget::onUpdate);

    QObject::connect(thread, &Solver::finished,
                     this, &Widget::onFinish);

    QObject::connect(thread, &Solver::finished,
                     thread, &Solver::deleteLater);

    thread->start();

//    while(thread->isRunning())
//    {

//    }
//    ui->optimizuj_pushButton->setEnabled(true);
}

